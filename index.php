<?php

    $to_do_list = ["item one","item two","item three","item four","item five"];
    
    function showItems(){
        global $to_do_list;
        return $to_do_list;
    }

    $list =  showItems();
    foreach($list as $item):
        echo "<ul>";
        echo "<li>".$item."</li>";
        echo "</ul>";
    endforeach;

    function changeItemList($index,$item){
        global $to_do_list;
        $to_do_list[$index] = $item;
        return $to_do_list;
    }

    $items = changeItemList(3,"new item");
    foreach($items as $newList):
        echo "<ul>";
        echo "<li>".$newList."</li>";
        echo "</ul>";
    endforeach;

    function removeItem($index){
        global $to_do_list;
        array_splice($to_do_list,$index,1);
        return $to_do_list;
    }

    $itemList = removeItem(3);
    foreach($itemList as $newList):
        echo "<ul>";
        echo "<li>".$newList."</li>";
        echo "</ul>";
    endforeach;
?>