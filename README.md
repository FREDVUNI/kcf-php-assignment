# PHP Assignment

This a simple PHP assignmentGetting Started

## Get started

To get started with the application, simply clone or download the repository to your local machine. The application requires a web server with PHP installed, so make sure that you have a web server set up.

Once you have the application files on your machine, you can start the application by navigating to the index.php file in your web browser.

## How it Works

- The application uses a global array called $to_do_list to store the list items. The array is initialized with 5 items, but can be modified as needed.
- Run `php fileName` to get results.

The application provides the following functions to manipulate the list:

`showItems()` - returns the current list of items.
`changeItemList($index, $item)` - replaces the item at the given index with the new item.
`removeItem($index)` - removes the item at the given index from the list.
These functions are called from the index.php file to add, edit, and delete items from the list.
